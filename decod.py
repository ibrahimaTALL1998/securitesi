import hashlib


# fonction qui hash une chaine de caractère
def hash(mdp, hash_mode):
    match hash_mode:
        case 'sha1':
            resultat = hashlib.sha1(mdp.encode())
            return resultat.hexdigest()
        case 'sha512':
            resultat = hashlib.sha512(mdp.encode())
            return resultat.hexdigest()
        case 'sha256':
            resultat = hashlib.sha256(mdp.encode())
            return resultat.hexdigest()
        case 'md5':
            resultat = hashlib.md5(mdp.encode())
            return resultat.hexdigest()


# si le mot de passe à des espaces, il va le traiter comme une liste
def preprocessing(password, hash_mode):
    password = password.split()
    new_password = []
    if len(password) > 1:
        for i in password:
            new_password.append(hash(i, hash_mode))
    return new_password


def inputs():
    # l'utilisateur entre le mot de passe à chercher
    #print("La fonction de hashage utilisée par défaut est le sha512")
    password = input('Entrez un mot de passe a décoder: ')

    # possibilité d'entrer un mot de passe déjà hasher. Si non, on peut choisir la fonction de hashage à appliquer
    choice = input('Est-ce déjà un hash?:'
                   '\n 1. OUI'
                   '\n 2. NON'
                   '\n')

    hash_mode = input('Entrez la fonction de hash à utiliser: '
                      '\n Taper soit'
                      '\nsha1'
                      '\nsha256'
                      '\nsha512'
                      '\nmd5'
                      '\n')

    verif = password.split()
    if choice == '2' and len(verif) == 1:
        password = hash(password, hash_mode)
    elif choice == '2' and len(verif) > 1:
        password = preprocessing(password, hash_mode)

    print('Mot de passe utilisé pour decoder aec la liste de mot : ',
          password)  # On lui affiche la chaine de caractères utilisées lors du traitement

    return (password, hash_mode, choice)


def bruteforce(word_list, mdp):
    counter = 0
    if isinstance(mdp, str):
        for password in word_list:
            if hash(password, hash_mode) == mdp:
                print("Le mot de passe est bien dans la liste de mot", "\n essayez-en un autre")
                # arreter le script s'il le trouve
                exit()
    else:
        for mot in mdp:
            print("mot in password", mot)
            for password in word_list:
                if hash(password, hash_mode) == mot:
                    counter = counter + 1
                    #print("counter", counter)
        if counter == len(mdp):
            print("Le mot de passe est bien dans la liste de mot", "\n essayez-en un autre")
            # arreter le script s'il le trouve
            exit()
    print('Mot de passe non trouvé')


info = open('info.txt', 'r')  # liste info
tr = open('tr.txt', 'r')  # liste tr

word_list = info.read() + '\n' + tr.read()  # liste des gens de DIC3

(password, hash_mode, choice) = inputs()

#on verifie si le mot de passe est dans la liste
bruteforce(word_list.split(), password)
